extends Area2D
var lvl = 1
var damage = 50
var knock_amount = 100
var attack_size = 1.0

var target = Vector2.ZERO
var angle = Vector2.ZERO

@onready var player = get_tree().get_first_node_in_group("player")
@onready var spellAnimation = $AnimationPlayer

func _ready():
	match lvl: #Asigna las stats del arma según el nivel del arma
		1:
			damage = 5 #damage = 5
			knock_amount = 100
			attack_size = 1.0
