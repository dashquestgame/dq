extends CharacterBody2D

signal mob_is_dead
signal can_shoot

@export var mob_exp = 100
@export var damage = 10
@export var speed = 100.0
@export var linear_velocity = 20.0
@export var hp = 1
@onready var loot_base = get_tree().get_first_node_in_group("loot")
@onready var player: Node = get_node("/root/Main/Player")
@onready var main: Node = get_node("/root/Main")

@onready var arrow = preload("res://Prefabs/Unlockable/Spells/Enemies_projectiles.tscn")

@onready var hitbox = $Hitbox
@onready var sprite = $Sprite2D
@onready var spriteAnimation = $AnimationPlayer
@onready var attack_coldown = $AttackColdown
@onready var projectile_coldown = $AttackColdown/ProjectileColdown
@onready var ArrowReference = $ArrowReference

@onready var mob_state = 2
var idle_end = true
var player_in_range = false
var shoot = true
var can_move = true

var arrowPositionFlipped = 0
var nonFlippedArrowPosition = 0

const IDLE = 0
const IS_DEATH = 1
const IS_RUNNING = 2
const Is_SHOOTING = 3

var expereience_orb = preload("res://Prefabs/ExpOrb.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():	
	arrowPositionFlipped = ArrowReference.position.x * -1
	nonFlippedArrowPosition = arrowPositionFlipped * -1
	spriteAnimation.play("run")
	hitbox.damage = damage
#	var mob_types = sprite.get_sprite_frames().get_animation_names()
#	sprite.animation = mob_types[randi() % mob_types.size()]

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
#	print(str(mob_state))
	#gets the position on of the player and moves towards at a certain velocity
	if(player!= null):
		var direction = global_position.direction_to(player.global_position)	
		if(can_move and mob_state!= IS_DEATH and shoot):
			spriteAnimation.pause()
			spriteAnimation.play("run")
			velocity =direction*speed
			# Changes the facing of the mob based on the direction
			move_and_slide()
		if direction.x >0.1:
			sprite.flip_h = false
			ArrowReference.position.x = nonFlippedArrowPosition
		elif direction.x < -0.1:
			sprite.flip_h = true
			ArrowReference.position.x = arrowPositionFlipped
			

			
func mob_death():
	var new_exp = expereience_orb.instantiate()
	new_exp.global_position = global_position
	loot_base.call_deferred("add_child", new_exp)
	main.current_enemies-=1
	
#func called when the mob receives damage and subtracts it from the hp. If the mob hp reaches 0, disappears.
func _on_hurtbox_hurt(damage):
	hp -= damage
	if hp<=0:
		mob_state = IS_DEATH
		
func _on_animation_player_animation_finished(anim_name):
	match anim_name:
		"death":
			mob_death()
			queue_free()
		"attack":
			shoot=true
			if(!can_move):
				spriteAnimation.pause()
				spriteAnimation.play("idle")

func _on_area_2d_body_entered(body):
	attack()
	player_in_range = true

func _on_area_2d_body_exited(body):
	can_move=true
	player_in_range = false
	
func attack():
	spriteAnimation.pause()
	spriteAnimation.play("attack")
	projectile_coldown.start()
	can_move=false
	print("algo")

func _on_timer_timeout():
	if player_in_range:
		attack()

func _on_projectile_coldown_timeout():
	var projectile = arrow.instantiate()
	projectile.position = ArrowReference.global_position
	projectile.scale = Vector2(2,2)
	projectile.target = Vector2(player.position.x,player.position.y)
	get_node("%Projectiles").add_child(projectile)
	attack_coldown.start()

func _on_animation_player_animation_started(anim_name):
	match anim_name:
		"attack":
			shoot=false


