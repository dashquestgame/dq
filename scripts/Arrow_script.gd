extends Area2D

var lvl = 1
var hp = 1
var speed = 100
var damage = 50
var knock_amount = 100
var attack_size = 1.0

var target = Vector2.ZERO
var angle = Vector2.ZERO

var projectile_hit = false

@onready var player = get_tree().get_first_node_in_group("player")
@onready var projectileAnimation = $AnimationPlayer

func _ready():
	projectileAnimation.play("fireball_default")
	angle = global_position.direction_to(target)
	rotation = angle.angle()
	match lvl: #Asigna las stats del arma según el nivel del arma
		1:
			hp = 1
			speed = 250
			damage = 5 #damage = 5
			knock_amount = 100
			attack_size = 1.0

func _physics_process(delta):
	position += angle*speed*delta
	
func enemy_hit(charge = 1):#elimina el proyectil al impactar con un enemigo
	hp -=charge
	if hp<= 0:
		queue_free()
#Elimina el proyectil una vez acabado el timer
func _on_timer_timeout():
	queue_free()

func _on_body_entered(body):
	var mob = str(body)
	mob = mob.substr(0, 6)
	match mob:
		"Player":
			queue_free()


func _on_animation_player_animation_finished(anim_name):
	match anim_name:	
		"fireball_default":
			projectileAnimation.pause()
			projectileAnimation.play("fireball_finish")
		
