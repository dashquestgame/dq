extends ColorRect

var mouse_over = false
var char = [""]

signal selected_character(char)

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("selected_character",Callable(PlayerStats,"select_character"))

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_mouse_entered():
	mouse_over = true

func _on_mouse_exited():
	mouse_over = false

#This method captures the click input on a character card,
#sends the choosen character to the PlayerStats script and
#starts the game with the choosen character
func _input(event):
	if event.is_action("click"):
		if mouse_over:
			char[0] = get_node("ClassName").text
			char.append( float(get_node("LifeIcon/LifeValue").text))
			char.append(float((get_node("WeaponDamage/DamageValue").text).substr(1,3)))
			char.append(float((get_node("SpellDamage/DamageValue").text).substr(1,3)))
			char.append(float((get_node("SpeedIcon/SpeedValue").text).substr(1,3)))
			char.append( float(get_node("ArmorIcon/ArmorValue").text))
			char.append(float((get_node("LuckIcon/LuckValue").text).substr(1,3)))
			emit_signal("selected_character",char)
			disconnect("selected_character",Callable(PlayerStats,"select_character"))
			get_tree().change_scene_to_file("res://main.tscn")
