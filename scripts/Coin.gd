extends Area2D

@export var coinValue = 30

var coin = preload("res://Prefabs/Coin.tscn")

var speed = -0.5
var acceleration = 2
var target = null

@onready var sprite = $Sprite2D
@onready var collision = $CollisionShape2D
@onready var sound = $Pickup_sound

# Called when the node enters the scene tree for the first time. 
# Changes the coin value at random between two numbers
func _ready():
	coinValue = randi_range(30,80)

# When detecting a target, the coin moves towards the player in a certain speed
func _process(delta):
	if target != null:
		global_position = global_position.move_toward(target.global_position, speed)
		speed += 2*delta*acceleration

#Func called when picking up a coin and plays the pick up sound, hides the it and returns the coin value 
func pickUp():
	sound.play()
	collision.call_deferred("set", "disabled", true)
	sprite.visible = false
	return coinValue

#When finishing the pickup sound, erases the item
func _on_pickup_sound_finished():
	queue_free()
