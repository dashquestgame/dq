extends Area2D

var lvl = 1
var hp = 1
var speed = 100
var damage = 50
var knock_amount = 100
var attack_size = 1.0
var stoppedTime = 3.0

var target = Vector2.RIGHT
var angle = Vector2.RIGHT

var projectile_hit = false

@onready var player = get_tree().get_first_node_in_group("player")
@onready var projectileAnimation = $AnimationPlayer

func _ready():
	projectileAnimation.play("fireball_default_2")
	angle = global_position.direction_to(target)
	match lvl: #Asigna las stats del arma según el nivel del arma
		1:
			hp = 1
			speed = 100
			damage = 500 #damage = 5
			knock_amount = 100
			attack_size = 1.0
			stoppedTime = 3.0
	get_node("StopTimer/DestroyTimer").wait_time = stoppedTime

func _physics_process(delta):
	position += angle*speed*delta
		
	
#func enemy_hit(charge = 1):#elimina el proyectil al impactar con un enemigo
#	hp -=charge
#	if hp<= 0:
#		queue_free()
		
#Stops the orb in place, and starts the destroy timer
func _on_timer_timeout():
	speed = 0
	get_node("StopTimer/DestroyTimer").start()

func _on_destroy_timer_timeout():
	queue_free()

func _on_body_entered(body):
	var mob = str(body)
	mob = mob.substr(1, 3)
#	match mob:
#		"Mob":
#			projectile_hit = true
#			projectileAnimation.play("fireball_finish")
#		"ob:":
#			projectile_hit = true			
#			projectileAnimation.play("fireball_finish")


func _on_animation_finished(anim_name):
	match anim_name:	
		"fireball_default_2":
			projectileAnimation.play("fireball_default")
		


