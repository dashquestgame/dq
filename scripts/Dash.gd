extends Node2D

const dash_delay = 1.5 


@onready var duration_timer = $Timer
var can_dash = true

func startDash(duration):

	duration_timer.wait_time = duration
	duration_timer.start()
	
func is_dashing():
	return duration_timer.is_stopped()
	
func end_dash():
	can_dash = false
	await get_tree().create_timer(dash_delay).timeout
	can_dash = true
	
func _on_timer_timeout():
	end_dash()
