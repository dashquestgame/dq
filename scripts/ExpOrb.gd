extends Area2D

@export var experience = 100

var blue_orb = preload("res://assets/Items/Orbs/XPOrb_blue.png")
var green_orb = preload("res://assets/Items/Orbs/XPOrb_green.png")
var yellow_orb = preload("res://assets/Items/Orbs/XPOrb_yellow.png")
var orange_orb = preload("res://assets/Items/Orbs/XPOrb_orange.png")
var purple_orb = preload("res://assets/Items/Orbs/XPOrb_purple.png")
var red_orb = preload("res://assets/Items/Orbs/XPOrb_red.png")

var speed = -0.5
var acceleration = 1.5
var target = null

@onready var sprite =$Sprite2D
@onready var collision = $CollisionShape2D
@onready var sound =$Pickup_sound

# Called when the node enters the scene tree for the first time.
func _ready():
	if experience < 11:
		return
	elif experience < 20:
		sprite.texture = green_orb
	elif experience < 50:
		sprite.texture = yellow_orb
	elif experience < 100:
		sprite.texture = orange_orb
	elif  experience < 200:
		sprite.texture = purple_orb
	else:
		sprite.texture = red_orb

# When detecting a target, the exp orb moves toward player's position in a certain speed
func _process(delta):
	if target != null:
		global_position = global_position.move_toward(target.global_position, speed)
		speed += 2*delta*acceleration

#Func called when picking up an the orb that plays the pick up sound, hides the orb and returns the orb's exp 
func pickUp():
	sound.play()
	collision.call_deferred("set", "disabled", true)
	sprite.visible = false
	return experience

#When finishing the pickup sound, erases the item
func _on_pickup_sound_finished():
	queue_free()
