extends Area2D

@export var healValue = 30

var food = preload("res://assets/drops/ChickenLeg.png")

var speed = -0.5
var acceleration = 2
var target = null

@onready var sprite = $Sprite2D
@onready var collision = $CollisionShape2D
@onready var sound = $Pickup_sound

# Called when the node enters the scene tree for the first time.
# Changes the food heal value at random between two numbers
func _ready():
	healValue = randi_range(20,40)

# When detecting a target, the food moves towards the player in a certain speed
func _process(delta):
	if target != null:
		global_position = global_position.move_toward(target.global_position, speed)
		speed += 2*delta*acceleration

#Func called when picking up a coin and plays the pick up sound, hides the it and returns the food value 
func pickUp():
	sound.play()
	collision.call_deferred("set", "disabled", true)
	sprite.visible = false
	return healValue

#When finishing the pickup sound, erases the item
func _on_pickup_sound_finished():
	queue_free()
