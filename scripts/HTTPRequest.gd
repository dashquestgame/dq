extends HTTPRequest
@onready var login_screen = get_parent()
@onready var register_screen = get_parent()
@onready var button = login_screen.get_node("PanelContainer/VBoxContainer/BtPlay")
@onready var errorLabel = login_screen.get_node("PanelContainer/VBoxContainer/IncorrectCredentials")
@onready var button2 = register_screen.get_node("PanelContainer/VBoxContainer/BtConfirm")
@onready var errorLabel2 = register_screen.get_node("PanelContainer/VBoxContainer/IncorrectCredentials")
var httpsDirection = "http://142.132.229.211:7070/login"
func _ready():
	pass


# Create an HTTP request node and connect its completion signal.
func http_post_request(username, password):
	var http_request = HTTPRequest.new()
	add_child(http_request)
	http_request.request_completed.connect(self._http_request_completed)
	# Perform a POST request. The URL below returns JSON as of writing.
	# Note: Don't make simultaneous requests using a single HTTPRequest node.
	# The snippet below is provided for reference only.
	var body = JSON.new().stringify({"username": username, "password": password})
	var error = http_request.request("http://142.132.229.211:7070/login", [], HTTPClient.METHOD_POST, body)
	if error != OK:
		push_error("An error occurred in the HTTP request.")
		errorLabel.text = "An error occurred in the HTTP request."
		errorLabel.modulate = Color(1,1,1,1) #cambiar opcaidad hacer visible label error 

# Create an HTTP request node and connect its completion signal.
func http_post_register_request(email, username, password):
	var http_request = HTTPRequest.new()
	add_child(http_request)
	http_request.request_completed.connect(self._http_request_completed2)
	# Perform a POST request. The URL below returns JSON as of writing.
	# Note: Don't make simultaneous requests using a single HTTPRequest node.
	# The snippet below is provided for reference only.
	var body = JSON.new().stringify({"email": email, "username": username, "password": password})
	var error = http_request.request("http://142.132.229.211:7070/register", [], HTTPClient.METHOD_POST, body)
	if error != OK:
		push_error("An error occurred in the HTTP request.")
		errorLabel2.text = "An error occurred in the HTTP request."
		errorLabel2.modulate = Color(1,1,1,1) #cambiar opcaidad hacer visible label error 

func _http_request_completed2(result, response_code, headers, body):
	var json = JSON.new()
	json.parse(body.get_string_from_utf8())
	var response = json.get_data()
	print(response)
	# Will print the user agent string used by the HTTPRequest node (as recognized by httpbin.org).
	if response != null:
		if response["data"]=="Validation OK":
			get_tree().change_scene_to_file("res://main_menu.tscn")
		else:
			errorLabel2.text = "Invalid credentials."
			errorLabel2.modulate = Color(1,1,1,1) #cambiar opcaidad hacer visible label error 
			button2.disabled = false
	else:
		errorLabel2.text = "Couldn't connect to server."
		errorLabel2.modulate = Color(1,1,1,1) #cambiar opcaidad hacer visible label error 
		button2.disabled = false

# Called when the HTTP request is completed.
func _http_request_completed(result, response_code, headers, body):
	var json = JSON.new()
	json.parse(body.get_string_from_utf8())
	var response = json.get_data()
	print(response)
	# Will print the user agent string used by the HTTPRequest node (as recognized by httpbin.org).
	if response != null:
		if response["data"]=="Validation OK":
			get_tree().change_scene_to_file("res://main_menu.tscn")
		else:
			errorLabel.text = "Invalid credentials."
			errorLabel.modulate = Color(1,1,1,1) #cambiar opcaidad hacer visible label error 
			button.disabled = false
	else:
		errorLabel.text = "Couldn't connect to server."
		errorLabel.modulate = Color(1,1,1,1) #cambiar opcaidad hacer visible label error 
		button.disabled = false
