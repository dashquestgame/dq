extends Control

@export var classes : Array[String] = [""] 

@export_enum("Knight", "Mage", "Axe", "Priest") var debug_parent: String

#Debug export for testing parent stat Inheritance
@onready var characters = get_node("MarginContainer/HBoxContainer")
@onready var current = get_node("%currPar")

var rand = RandomNumberGenerator.new()

signal selected_character(character)


# Called when the node enters the scene tree for the first time.
func _ready():
	
	current.text = ("Parent: "+debug_parent)
	testGenerateClasses()

func _unhandled_input(event):
	if event.is_action_pressed("debug_ui"):
		testGenerateClasses()
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

#This method test the function to roll new characters every time, it also randomizes the parent for debug purposes
func testGenerateClasses():
	debug_parent = randomize_parent()
	current.text = ("Parent: "+debug_parent)
	
	var dict = {}
	
	var f = FileAccess.open("res://assets/classes.json", FileAccess.READ)
	var text = f.get_as_text()
	dict = JSON.parse_string(text)

	for x in characters.get_child_count():
		var char = characters.get_child(x)
		var character_class = rand.randi_range(0, (classes.size()-1))
		
		for c in dict["classes"]:
			if (c["name"] == classes[character_class]):
				char.get_node("ClassName").text = c["name"]
				char.get_node("LifeIcon").get_node("LifeValue").text = c["health"]
				char.get_node("WeaponDamage").get_node("DamageValue").text = "x"+c["weapon_dmg"]
				char.get_node("SpellDamage").get_node("DamageValue").text = "x"+c["spell_dmg"]
				char.get_node("SpeedIcon").get_node("SpeedValue").text = "x"+c["speed"]
				char.get_node("ArmorIcon").get_node("ArmorValue").text = c["armour"]
				char.get_node("LuckIcon").get_node("LuckValue").text = "x"+c["luck"]	
				var auxPortrait = "res://assets/portraits/%s_portrait.png"
				var portrait = auxPortrait % c["name"]
				char.get_node("ClassIcon").texture = load(portrait)
				
				clearBuffs(char,c)
				match debug_parent:
					"Knight":
						buffStat(char,c,"weapon_dmg",0.2)
					"Mage":
						buffStat(char,c,"spell_dmg",0.2)
					"Axe":
						buffStat(char,c,"weapon_dmg",0.2)
					"Priest":
						buffStat(char,c,"spell_dmg",0.2)
				
		classes.erase(classes[character_class])
		
		
	classes = ["Knight","Axe","Mage","Priest"]


#Debug method for testing the Parent inheritance system
func randomize_parent():
	var r = randi_range(0,3)
	var aux = ""
	match r:
		0:
			aux = "Knight"
		1:
			aux = "Axe"
		2:
			aux = "Priest"
		3:
			aux = "Mage"
	return aux
	
#Sets all the labels to white	
func clearBuffs(char, c):
		char.get_node("WeaponDamage").get_node("DamageValue").modulate = Color(1,1,1)
		char.get_node("SpellDamage").get_node("DamageValue").modulate = Color(1,1,1)
		char.get_node("SpeedIcon").get_node("SpeedValue").modulate = Color(1,1,1)
		char.get_node("ArmorIcon").get_node("ArmorValue").modulate = Color(1,1,1)
		char.get_node("LuckIcon").get_node("LuckValue").modulate = Color(1,1,1)	
		
func buffStat(char, c, stat, value):
	match stat:
		"weapon_dmg":
			var aux = float(c["weapon_dmg"])+0.2
			char.get_node("WeaponDamage").get_node("DamageValue").text = "x"+str(aux)
			char.get_node("WeaponDamage").get_node("DamageValue").modulate = Color(0,1,0)
		"spell_dmg":
			var aux = float(c["spell_dmg"])+0.2
			char.get_node("SpellDamage").get_node("DamageValue").text ="x"+str(aux)
			char.get_node("SpellDamage").get_node("DamageValue").modulate = Color(0,1,0)
	
