extends Node

@export var weapons : Array[String] = [] 
@export var passiveIncrease : Array[String] = [] 

var auxUpgrades = []

var currentItems = {"Placeholder":0}

#Spells
var magicBolt = preload("res://Prefabs/Unlockable/Spells/MagicBolt.tscn")
var fireball = preload("res://Prefabs/Unlockable/Spells/Fireball.tscn")
var cosmicCluster = preload("res://Prefabs/Unlockable/Spells/CosmicCluster.tscn")
var darkDomain = preload("res://Prefabs/Unlockable/Spells/DarkDomain.tscn")
var darkDomainNode = darkDomain.instantiate()
var fireballNode = fireball.instantiate()
var magicBoltNode = magicBolt.instantiate()
var cosmicClusterNode = cosmicCluster.instantiate()

#Weapons
var spear = preload("res://Prefabs/Unlockable/Weapons/Spear.tscn")
var sword = preload("res://Prefabs/Unlockable/Weapons/Sword.tscn")
var spearNode = spear.instantiate()
var swordNode = sword.instantiate()

#Passives
var ancientArmour = preload("res://Prefabs/Unlockable/Passives/AncientArmour.tscn")
var ancientArmourNode = ancientArmour.instantiate()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

# Function called every time player selects an already unlocked item
#it handles leveling up items and manages conflict with already max level items
func upgradeItem(item):
	var aux = currentItems.get(item) as int
	ItemController.currentItems[item] = aux+1
	
# Function that manages unlocking items in a run
func addItem(item):
	ItemController.currentItems[item] = 1
	attachItem(item)

# Function that attaches an unlocked item to the player node
func attachItem(item):
	var player = get_tree().get_first_node_in_group("player")
	match item:
		"Fireball":
			player.get_node("UnlockedItems").add_child(fireballNode)
		"Magic Bolt":
			player.get_node("UnlockedItems").add_child(magicBoltNode)
		"Cosmic Cluster":
			player.get_node("UnlockedItems").add_child(cosmicClusterNode)
		"Dark Domain":
			player.get_node("UnlockedItems").add_child(darkDomainNode)
		"Spear":
			player.get_node("UnlockedItems").add_child(spearNode)
		"Sword":
			player.get_node("UnlockedItems").add_child(swordNode)
		"Ancient Armour":
			player.get_node("UnlockedItems").add_child(ancientArmourNode)

# Function that returns a random unlockable weapon
func getRandomWeapon():		
	var weapon = weapons.pick_random()
	auxUpgrades.append(weapon)
	return weapon

# Function that returns a random unlockable passive increase
func getRandomPassiveItem():
	return passiveIncrease.pick_random()
	
#TODO: Random StatUp
