extends ColorRect

@onready var player = get_tree().get_first_node_in_group("player")

#debug upgrade
@onready var fireball = preload("res://Prefabs/Unlockable/Spells/Fireball.tscn")

var mouse_over = false
var item = null

#Labels for dinamically choices
@onready var nameLabel = get_node("lbl_name")
@onready var descLabel = get_node("lbl_desc")
@onready var levelLabel = get_node("lbl_level")

signal selected_upgrade(upgrade)

func _ready():
	connect("selected_upgrade",Callable(player,"upgrade_character"))
#	setItem(item)
	
func _on_mouse_entered():
	mouse_over = true

func _on_mouse_exited():
	mouse_over = false

func _input(event):
	if event.is_action("click"):
		if mouse_over:
			item = nameLabel.text
			if !ItemController.currentItems.has(item):
				ItemController.addItem(item) #Adds item to the unlockedItems list
			else:
				ItemController.upgradeItem(item) #Upgrades item from unlockedItems list
			emit_signal("selected_upgrade",item)
#itemSetter			
#TODO: Open Unlockables.JSON and parse the recieved item
#Set labels and texture via JSON params
func setItem(item):
	var dict = {}
	
	var f = FileAccess.open("res://assets/Unlockables.json", FileAccess.READ)
	var text = f.get_as_text()
	dict = JSON.parse_string(text)
	
	for x in dict.size():
			nameLabel.text = dict[item]["display_Name"]
			if !ItemController.currentItems.has(item):
				descLabel.text = dict[item]["levels"]["1"]["details"]
			else:
				var aux = ItemController.currentItems.get(item) as int
				if dict[item]["levels"].size() >= (aux+1) as int:
					descLabel.text = dict[item]["levels"][str(aux+1)]["details"]
					levelLabel.text = str("Level: ",aux+1)
				else:
					levelLabel.text = "Awakened"
			var sprite : TextureRect = get_node("ItemImage/ItemSprite") 
			var spriteLocation = ("res://Atlas/Unlockables/%s.tres" % item)
			sprite.texture = load(spriteLocation)
