extends Area2D
var lvl = 1
var hp = 1
var speed = 150
var damage = 20
var knock_amount = 100
var attack_size = 1.0

var target = Vector2.ZERO
var angle = Vector2.ZERO

var projectile_hit = false

@onready var player = get_tree().get_first_node_in_group("player")
@onready var projectileAnimation = $AnimationPlayer

func _ready():
	angle = global_position.direction_to(target)
	rotation = angle.angle() + deg_to_rad(135)
	match lvl: #Asigna las stats del arma según el nivel del arma
		1:
			hp = 1
			speed = 100
			damage = 500 #damage = 5
			knock_amount = 100
			attack_size = 1.0

func _physics_process(delta):
	position += angle*speed*delta
	if !projectile_hit:
		projectileAnimation.play("magicbolt_default")

func enemy_hit(charge = 1):#elimina el proyectil al impactar con un enemigo
	hp -=charge
	if hp<= 0:
		queue_free()

#Elimina el proyectil una vez acabado el timer
func _on_timer_timeout():
	queue_free()

func _on_body_entered(body):
	var mob = str(body)
	mob = mob.substr(1, 3)
