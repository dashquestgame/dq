extends Node
@export var mob_scene = preload("res://Prefabs/mob.tscn")
@export var max_enemies_spawn = 300
var current_enemies = 0
@onready var player = get_tree().get_first_node_in_group("player")
@onready var mob_spawn_location = get_node("Player/Camera2D/MobPath/MobSpawnLocation")

@onready var gameOverCanvasLayer = $GameOverCanvasLayer
@onready var mobTimer = $MobTimer
@onready var playerStartPosition = $StartPosition
@onready var startTimer = $StartTimer

signal takedown

var state_world = 0

const GAME_PAUSE = 1
const GAME_RUNNING = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	new_game()

func _unhandled_input(event):
	if event.is_action_pressed("ui_accept") and gameOverCanvasLayer.visible:		# This restarts the current scene.
		get_tree().reload_current_scene()
	if event.is_action_pressed("esc") and state_world == GAME_RUNNING:
		get_tree().paused = true
	else:
		get_tree().paused = false

func _on_player_death():
	gameOverCanvasLayer.show()	
	mobTimer.stop()

func new_game():
	gameOverCanvasLayer.hide()
	player.start(playerStartPosition.position)
	startTimer.start()

func _on_mob_timer_timeout():
	# Create a new instance of the Mob scene.
	if current_enemies < max_enemies_spawn:
		if(mob_spawn_location!=null):
			var mob = mob_scene.instantiate()
			# Choose a random location on Path2D.
			var rng = RandomNumberGenerator.new()
			var rand_number = rng.randf()
			mob_spawn_location.progress_ratio = rand_number
			# Set the mob's position to a random location.
			mob.position = mob_spawn_location.global_position
			# Choose the velocity for the mob.
			var velocity = Vector2(mob.speed,0)
			# Spawn the mob by adding it to the Main scene.
			current_enemies+=1
			mob.connect("mob_is_dead",on_mob_mob_is_dead)
			add_child(mob)
		
func _on_start_timer_timeout():
	mobTimer.start()
	
func on_mob_mob_is_dead():
	current_enemies-=1
	emit_signal("takedown")
