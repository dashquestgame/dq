extends Control

var selected_button = null
@onready var panelContainer = $PanelContainer/MarginContainer2/ButtonContainer/BtPlay

@onready var Inh = load("res://Screens/Inheritance.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	panelContainer.grab_focus()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_bt_play_pressed():
	get_tree().change_scene_to_file("res://Screens/Inheritance.tscn")


func _on_bt_shop_pressed():
	pass # Replace with function body.


func _on_bt_collection_pressed():
	pass # Replace with function body.


func _on_bt_achievements_pressed():
	pass # Replace with function body.

func _on_bt_leaderboard_pressed():
	pass # Replace with function body.


func _on_bt_settings_pressed():
	pass # Replace with function body.


func _on_bt_credits_pressed():
	pass # Replace with function body.


func _on_bt_quit_pressed():
	get_tree().quit()


func _on_button_focused(button):
	if selected_button != null:
		selected_button.pressed = false    
	selected_button = button


func _on_mouse_entered():
	grab_focus()
