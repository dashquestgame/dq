extends CharacterBody2D

signal mob_is_dead

@export var mob_exp = 100
@export var damage = 10
@export var speed = 50.0
@export var linear_velocity = 20.0
@export var hp = 10
@onready var loot_base = get_tree().get_first_node_in_group("loot")
@onready var player: Node = get_node("/root/Main/Player")
@onready var main: Node = get_node("/root/Main")

@onready var hitbox = $Hitbox
@onready var sprite = $Sprite2D
@onready var spriteAnimation = $AnimationPlayer
@onready var mob_state = 0

const IDLE = 0
const IS_DEATH = 1
#LOOT 
var food = preload("res://Prefabs/food.tscn")
var expereience_orb = preload("res://Prefabs/ExpOrb.tscn")
var coin = preload("res://Prefabs/Coin.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	hitbox.damage = damage
#	var mob_types = sprite.get_sprite_frames().get_animation_names()
#	sprite.animation = mob_types[randi() % mob_types.size()]

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	#gets the position on of the player and moves towards at a certain velocity
	
	match mob_state:
		IDLE:
			spriteAnimation.play("idle")
			if(player!= null):
				var direction = global_position.direction_to(player.global_position)
				velocity =direction*speed
				move_and_slide()
				# Changes the facing of the mob based on the direction
				if direction.x >0.1:
					sprite.flip_h = false
				elif direction.x < -0.1:
					sprite.flip_h = true
		IS_DEATH:
			spriteAnimation.play("death")

#When the mob dies, emits a signal, generates an exp orb and has a chance of dropping coins and food.
func mob_death():
	emit_signal("mob_is_dead")
	
	var new_exp = expereience_orb.instantiate()
	new_exp.global_position = global_position
	loot_base.call_deferred("add_child", new_exp)
	
	var healDropChance = randi_range(0,300)
	if healDropChance-player.luck <= 5:
		var new_food = food.instantiate()
		var random_offset1 = Vector2(randf_range(-2, 2), randf_range(-2, 2)) * 10 #spawns the food at a random position close to the mob's global position
		new_food.global_position = global_position + random_offset1
		loot_base.call_deferred("add_child", new_food)
	
	var coinDropChance = randi_range(0,250)
	if coinDropChance-player.luck <= 5:
		var new_coin = coin.instantiate()
		var random_offset2 = Vector2(randf_range(-2, 2), randf_range(-2, 2)) * 10
		new_coin.global_position = global_position + random_offset2
		loot_base.call_deferred("add_child", new_coin)

#func called when the mob receives damage and subtracts it from the hp. If the mob hp reaches 0, disappears.
func _on_hurtbox_hurt(damage):
	hp -= damage
	if hp<=0:
		mob_death()
		mob_state = IS_DEATH

func _on_animation_player_animation_finished(anim_name):
	match anim_name:
		"death":
			call_deferred("queue_free")

