extends Path2D

var enemy = preload("res://Prefabs/mob.tscn")
@onready var mobSpawnLocation = $Player/MobPath/MobSpawnLocation
@onready var mobSpawnPosition2D = $Player/MobPath/MobSpawnLocation/Position2D


func _ready():
	randomize()

func _on_EnemyTiemr_timeout():
	var rng = RandomNumberGenerator .new( )

	mobSpawnLocation.offset = rng.randi_range(0,3664)
	var instance = enemy.instance()
	instance.global_position = mobSpawnPosition2D.global_position
	add_child( instance)
