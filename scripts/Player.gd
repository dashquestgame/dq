extends CharacterBody2D
signal death
signal level_up
signal get_exp(experience)
signal get_heal(heal_amount)
signal get_coin(coin_amount)

@export var luck = 2
@export var total_hp = 100 
@export var current_hp = 100
@export var armor = 1
@export var move_speed = 300
@export var dash_speed = 150
@export var dash_duration = 0.3
var experiencia = 0
var direction = "none"
var character_pos

var has_spear = false

#Exp related
var experience = 0
var level = 1
var levelCap = 50
var current_experience = 0

var playerStateForAnimation = 0

const IDLE = 0
const IS_MOVING = 1
const IS_DASHING = 2
const IS_DEATH = 3

@onready var itemController = get_tree().get_first_node_in_group("iController")

var totalCoins = 0
var totalTakedowns = 0

#Enemies
var enemy_close = []

#Dash node 
@onready var dash = get_node("%Dash")
#Weapons node
@onready var weapons = get_node("%Weapons")
#Spells node
@onready var spells = get_node("%Spells")

#Sprint 2 Alessandro
#GUI
@onready var takedownsLabel = $GUILayer/GUI/VBoxContainer/KillCount_label
@onready var coinsLabel = $GUILayer/GUI/VBoxContainer/Coins_label
@onready var playerSprite = $Sprite2D
@onready var playerCollision = $CollisionShape2D
@onready var playerHealthBar = $HealthBar
@onready var playerAnimation = $AnimationPlayer

#@onready var weaponAnimation = $Player/Weapons/Spear/WeaponAnimations

@onready var exp_bar = get_node("%Level_progress_bar")
@onready var player_lvl_label = get_node("%Level_label")
@onready var levelPanel = get_node("%LevelUp")
@onready var upgradeOptions = get_node("%UpgradeOptions")
@onready var lvlUpSound = get_node("%lvlUpSound")

#LevelUp 
@onready var itemOptions = preload("res://Prefabs/item_option.tscn")

@onready var weaponList = get_tree().get_first_node_in_group("WeaponList")
@onready var spellList = get_tree().get_first_node_in_group("SpellList")

#Animations
var formatRun = "%s_run"
var formatIdle = "%s_idle"
var formatDash = "%s_dash"
var formatDeath = "%s_death"

var a_run = ""
var a_idle = ""
var a_dash = ""
var a_death = ""

# Called when the node enters the scene tree for the first time.
func _ready():
	set_character(PlayerStats.playerClass)
#Function that gets the move input
func get_input():
	velocity = Vector2()
	
	match playerStateForAnimation:
		IS_MOVING:
			if dash.is_dashing() and playerStateForAnimation != IS_DEATH:
				playerAnimation.play(a_run)
		IDLE:
			if playerStateForAnimation != IS_DEATH:
				playerAnimation.play(a_idle)
		IS_DASHING:
			playerAnimation.play(a_dash)
		IS_DEATH:
			playerAnimation.play(a_death)		
			

	if Input.is_action_pressed("right"):
		velocity.x += 1
		playerSprite.flip_h = false
		if playerStateForAnimation != IS_DEATH:
			playerStateForAnimation = IS_MOVING
	if Input.is_action_pressed("left"):
		velocity.x -= 1
		playerSprite.flip_h = true
		if playerStateForAnimation != IS_DEATH:
			playerStateForAnimation = IS_MOVING
	if Input.is_action_pressed("down"):
		velocity.y += 1
		if playerStateForAnimation != IS_DEATH:
			playerStateForAnimation = IS_MOVING
	if Input.is_action_pressed("up"):
		velocity.y -= 1
		if playerStateForAnimation != IS_DEATH:
			playerStateForAnimation = IS_MOVING
	if Input.is_action_pressed("dash") and dash.can_dash and dash.is_dashing():
		dash.startDash(dash_duration)
		if playerStateForAnimation != IS_DEATH:
			playerStateForAnimation = IS_DASHING
	if Input.is_action_pressed("debug_ui"):
		playerStateForAnimation = IS_DEATH
	
	if velocity == Vector2.ZERO and dash.is_dashing() and playerStateForAnimation != IS_DEATH:
		playerStateForAnimation = IDLE

	saveDirection()
	
#Method that stores the direction the player is moving on a variable
func saveDirection():
	if (velocity.x > 0 and velocity.y > 0):
		direction = "down_right"
	if (velocity.x < 0 and velocity.y > 0):
		direction = "down_left"
	if (velocity.x > 0 and velocity.y < 0):
		direction = "up_right"
	if (velocity.x < 0 and velocity.y < 0):
		direction = "up_left"
	if (velocity.x > 0 and velocity.y == 0):
		direction = "right"
	if (velocity.x < 0 and velocity.y == 0):
		direction = "left"	
	if (velocity.x == 0 and velocity.y > 0):
		direction = "down"
	if (velocity.x == 0 and velocity.y < 0):
		direction = "up"

#Moves the player based on the input received
func _physics_process(delta):
	get_input()
	var speed = dash_speed if dash.is_dashing() else move_speed
	velocity = velocity.normalized() * speed
	if playerStateForAnimation != IS_DEATH:
		move_and_slide()
	character_pos = get_transform()
	
#	var weaponList = WeaponList.get_children()

#spawns the player at start position 
func start(pos):
	position = pos
	show()
	playerCollision.disabled = false

#Handles in wich direction the Spear should attack
func _on_weapon_attack(weapon):
	print(weapon)
	weapon.play_animation(direction)

#Triggered when player is hurt
func _on_hurtbox_hurt(damage):
	if !dash.is_dashing(): return
	playerHealthBar.value-=damage
	if playerHealthBar.value<=0:
		playerStateForAnimation = IS_DEATH
		
func _player_death():
	queue_free()
	death.emit()

#Func that gets a close random enemy target to shoot at, returning its global position
func get_random_target():
	if enemy_close.size() > 0:
		return enemy_close.pick_random().global_position
	else:
		return Vector2.UP
		
#Func that gets the closest enemy target to shoot at, returning its global position	
func get_closest_target():
	var closest_mob = null
	var closest_dist = 9999999
	if enemy_close.size() > 0:
		for mob in enemy_close:
			var dist = self.position.distance_to(mob.position)
			if dist < closest_dist:
				closest_dist = dist
				closest_mob = mob
		return closest_mob.global_position
	else:
		return Vector2.UP

#Func that adds the body that entered the detection range to the array   
func _on_enemy_detection_range_body_entered(body):
	if not enemy_close.has(body):
		enemy_close.append(body)
 
#omaigat
#Func that erases the body that exited the detection range from the array   
func _on_enemy_detection_range_body_exited(body):
	if enemy_close.has(body):
		enemy_close.erase(body)

#When loot enters the pickup area, makes the item target the player
func _on_pickup_range_area_entered(area):
	if area.is_in_group("loot"):
		area.target = self

#When loot enters the pickup area, makes the item target the player
func _on_collect_range_area_entered(area):
	if area.is_in_group("exp"):
		var orb_exp = area.pickUp()
		emit_signal("get_exp", orb_exp)
	if area.is_in_group("food"):
		var heal_amount = area.pickUp()
		emit_signal("get_heal", heal_amount)
	if area.is_in_group("coin"):
		var coin_amount = area.pickUp()
		emit_signal("get_coin", coin_amount)
		
#Function that is called when our playable character levels up #Sprint 2 Alessandro
#it shows the upgrade selection screen and pauses the game
func upgrade_character(upgrade):
	var option_children = upgradeOptions.get_children()
	for i in option_children:
		i.queue_free()
	levelPanel.visible = false
	get_tree().paused = false
	ItemController.attachItem(upgrade)
#	ItemController.attachItem("Ancient Armour")
	recalculateStats()
	_on_get_exp(0)

func level_up_menu():
	levelPanel.position.y=-800
	#hacer que suene el sonido de subir de nivel
	player_lvl_label.text = "Level: %s" % level
	var tween = levelPanel.create_tween()
	tween.tween_property(levelPanel, "position", Vector2(0,0),0.2).set_trans(Tween.TRANS_LINEAR).set_ease(Tween.EASE_IN)
	tween.play()
	levelPanel.visible = true
	
	var options = 0
	var optionsmax = 4
	while options < optionsmax:
		var choiceName = itemController.getRandomWeapon() ##TODO: IF no devuelve null options+1
		var option_choice = itemOptions.instantiate()
		#Llamar a Weapon/Spell/Passive List para obtener una mejora aleatoria
		#Establecer option_choice con su nuevo valor
		upgradeOptions.add_child(option_choice)
		option_choice.setItem(choiceName)
		options+=1	
	get_tree().paused = true


func _on_get_exp(experience):
	current_experience += experience
	if (current_experience >= exp_bar.max_value):
		level += 1
		current_experience = current_experience-levelCap
		levelCap+=(levelCap*0.25)
		exp_bar.max_value = levelCap
		exp_bar.value = 0
		level_up_menu()
	exp_bar.value = current_experience


func _on_animation_player_animation_finished(anim_name):
	if (anim_name == a_death):
		_player_death()
	
# Function that sets the animations for the selected players		
func set_character(playerName):
	a_idle = formatIdle % playerName
	a_run = formatRun % playerName
	a_dash = formatDash % playerName
	a_death = formatDeath % playerName
	
	total_hp = PlayerStats.playerHealth
	armor = PlayerStats.playerArmor
	move_speed = move_speed * PlayerStats.playerSpeed
	dash_speed = dash_speed * PlayerStats.playerSpeed
	
# Function that is called each time an update is selected 
# Handles stats of the player beign changed and updates them
func recalculateStats():
	print(current_hp)
	var aux = PlayerStats.playerHealth - total_hp
	total_hp = PlayerStats.playerHealth
	current_hp += aux
	armor = PlayerStats.playerArmor
	move_speed = move_speed * PlayerStats.playerSpeed
	print(current_hp)

#Method called when the player picks up food. If the heal value is greater than player's total hp,
# the health bar is fully restored without exceeding the limit. If not, it restores that amount.
func _on_get_heal(heal_amount):
	if playerHealthBar.value + heal_amount > total_hp:
		playerHealthBar.value = total_hp
	else:
		playerHealthBar.value += heal_amount

#Method called when the player picks up a coin and adds the coin value to a variable that stores
#every coin earned in a run and shows it on the coin label.
func _on_get_coin(coin_amount):
	totalCoins += coin_amount
	coinsLabel.text = str(totalCoins)

#Method called when the player kills a mob and shows the total takedowns in the takedowns label
func _on_main_takedown():
	totalTakedowns += 1
	takedownsLabel.text = str(totalTakedowns)
	print(totalTakedowns)
