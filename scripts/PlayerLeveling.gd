extends Node2D

var levelCap = 50

signal levelup;

@onready var exp_bar = $Level_progress_bar
@onready var player_level_label = $Level_progress_bar/Level_label

@onready var levelPanel = get_node("%LevelUp")
@onready var upgradeOptions = get_node("%UpgradeOptions")

#LevelUp 
@onready var itemOptions = preload("res://Prefabs/item_option.tscn")

func _ready():
	exp_bar.max_value = levelCap

func _on_player_get_exp(experience):
	exp_bar.value += experience
	if (exp_bar.value == exp_bar.max_value):
		player_level_label.level_up()
		levelCap+=(levelCap*1.25)
		exp_bar.max_value = levelCap
		exp_bar.value = 0
		emit_signal("levelup")
		
func level_up():
	levelPanel.visible = true
	var options = 0
	var optionsmax = 4
	while options < optionsmax:
		var option_choice = itemOptions.instantiate()
		upgradeOptions.add_child(option_choice)
		options+=1
		
	get_tree().paused = true
