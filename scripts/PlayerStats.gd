extends Node

var playerClass = "Priest"

signal recalculateStats()

var playerHealth = 0
var playerWeaponDamage = 0
var playerSpellDamage = 0
var playerSpeed = 0
var playerArmor = 0
var playerLuck = 0


#Method for handling selecting a player
func select_character(character):
	playerClass = character[0]
	playerHealth = character[1] 
	playerWeaponDamage = character[2] 
	playerSpellDamage = character[3] 
	playerSpeed = character[4] 
	playerArmor = character[5] 
	playerLuck = character[6] 
	
	print("playerHealth "+str(playerHealth))
	
func addIncrease(stat, increase):
	print("Adding: "+stat +" for a value of " + str(increase))
	match stat:
		"Health":
			playerHealth += increase
		"Armour":
			playerArmor += increase
		"WeaponAttack":
			playerWeaponDamage += increase
		"SpellAttack":
			playerSpellDamage += increase
		"Speed":
			playerSpeed += increase
		
func addMultiplier(stat, multiplier):
	print("Adding Multiplier to: "+stat +" for a value of " + str(multiplier))

