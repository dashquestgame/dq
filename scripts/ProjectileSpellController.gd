extends Node

@onready var player = get_tree().get_first_node_in_group("player")
@onready var spellNode = preload("res://Prefabs/Unlockable/Spells/FireballProjectile.tscn")

#Timers
@onready var spellTimer = get_node("%SpellTimer")
@onready var spellAttackTimer = get_node("%SpellAttackTimer")

@export_multiline var description
@export var spellName = ""
@export_global_file("*.png") var spellImage

#Fireball
var spell_ammo = 0
var spell_baseammo = 1
@export var spell_atkspeed = 2
@export var spell_lvl = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	match spellName:
		"Fireball":
			spellNode = preload("res://Prefabs/Unlockable/Spells/FireballProjectile.tscn")
		"Cosmic Cluster":
			spellNode = preload("res://Prefabs/Unlockable/Spells/CosmicClusterProjectile.tscn")
			spell_atkspeed = 3.5
	attack()

func attack():
	if spell_lvl>0:
		spellTimer.wait_time = spell_atkspeed
		if spellTimer.is_stopped():
			spellTimer.start()

#When on spellTimer timeout, we add the base ammo to fireball ammo and we start the attack timer
func _on_spell_timer_timeout():
	spell_ammo += spell_baseammo
	spellAttackTimer.start()
	
#When on spellAttackTimer timeout, if the ammo is higher than 0, we instantiate a fireball, spawning it at the current
#position, giving it a random enemy target and adjusting its level. After the attack, we substrack the ammo used and if it is higher
#than 0, we start the timer again, if not, we stop it.
func _on_spell_attack_timer_timeout():
	if spell_ammo > 0:
		#print("Eeeeexplosion")
		var spell_attack = spellNode.instantiate()
		spell_attack.position = player.position
		spell_attack.target = player.get_random_target()
		spell_attack.lvl = spell_lvl
		add_child(spell_attack)
		spell_ammo -=1
		if spell_ammo>0:
			spellAttackTimer.start()
		else:
			spellAttackTimer.stop()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
