extends MeshInstance2D


# Called when the node enters the scene tree for the first time.
func _ready():
	var st = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_TRIANGLES)

	var inner_radius = 25.0
	var outer_radius = 50.0
	var sides = 32
	var rings = 8

	for r in range(rings):
		for s in range(sides):
			var angle = s / float(sides) * TAU
			var cos_angle = cos(angle)
			var sin_angle = sin(angle)
			var x = (outer_radius + inner_radius * cos(cos_angle)) * cos(sin_angle)
			var y = (outer_radius + inner_radius * cos(cos_angle)) * sin(sin_angle)	        
			st.add_uv(Vector2(x, y))
			
			var next_s = (s + 1) % sides
			var next_r = (r + 1) % rings
			var v1 = r * sides + s
			var v2 = next_r * sides + s
			var v3 = r * sides + next_s
			var v4 = next_r * sides + next_s
			st.add_triangle(v1, v3, v2)
			st.add_triangle(v2, v3, v4)
	st.generate_normals()
	var mesh = st.commit()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
