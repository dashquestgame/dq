extends Node
#This script will store all the statistic increment upgrades the player can get
#and will handle a selected spell upgrade to the player when needed

#List of all the statistic increment types as prefabs
@export var spear = preload("res://Prefabs/Unlockable/Weapons/Spear.tscn")
#Instance of each spell type
var spearNode = spear.instantiate()

@onready var player = get_tree().get_first_node_in_group("player")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func attachUpgrade(upgrade):
	match upgrade:
		"Spear":
			player.get_node("Weapons").add_child(spearNode)
