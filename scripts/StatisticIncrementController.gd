extends Sprite2D

@export var amount = 0
@export_enum("Increase", "Multiplier") var type = 0
@export_enum("Health", "Armour", "WeaponAttack", "SpellAttack", "Speed") var increasedStat = 0

@export_multiline var description

# Called when the node enters the scene tree for the first time.
func _ready():
	match type:
		0: #Increase Upgrades
			if (increasedStat == 0):
				PlayerStats.addIncrease("Health",amount)
			if (increasedStat == 1):
				PlayerStats.addIncrease("Armour",amount)
			if (increasedStat == 2):
				PlayerStats.addIncrease("WeaponAttack",amount)
			if (increasedStat == 3):
				PlayerStats.addIncrease("SpellAttack",amount)
			if (increasedStat == 4):
				PlayerStats.addIncrease("Speed",amount)
		1: #Multiplier Upgrades
			if (increasedStat == 0):
				print("Health Multiplier")
			if (increasedStat == 1):
				print("Armour Multiplier")
			if (increasedStat == 2):
				print("WeaponAttack Multiplier")
			if (increasedStat == 3):
				print("SpellAttack Multiplier")
			if (increasedStat == 4):
				print("Speed Multiplier")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
