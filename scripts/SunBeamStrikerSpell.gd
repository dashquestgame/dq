extends Area2D

var lvl = 1
var damage = 50
var knock_amount = 100
var attack_size = 1.0

var target = Vector2.ZERO
var angle = Vector2.ZERO

var projectile_hit = false

@onready var player = get_tree().get_first_node_in_group("player")
@onready var projectileAnimation = $AnimationPlayer

func _ready():
	angle = global_position.direction_to(target)
	match lvl: #Asigna las stats del arma según el nivel del arma
		1:
			damage = 500 #damage = 5
			knock_amount = 100
			attack_size = 1.0

#Elimina el proyectil una vez acabado el timer
func _on_timer_timeout():
	queue_free()

func _on_animation_player_animation_finished(anim_name):
	match anim_name:	
		"beam_animation":
			queue_free()
