extends Node

signal  attack
@onready var player = get_tree().get_first_node_in_group("player")
@export var weaponName = "Placeholder"
@export var cooldown: float = 1
@export var attackDuration: float = 1
@export var knockback : float = 0
@export_global_file("*.png") var sprite

@onready var attackCooldown = $AttackCooldown
@onready var animationTimer = $AnimationTimer

@onready var weaponAnimation = get_tree().get_first_node_in_group("Anim")


# Called when the node enters the scene tree for the first time.
func _ready():
	connect("attack",Callable(player,"_on_weapon_attack"))
	_reset_attack_timer()

func _reset_attack_timer():
	attackCooldown.stop()
	attackCooldown.wait_time = cooldown
	attackCooldown.start()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	get_node("WeaponNode").position = player.position

func _on_animation_timer_timeout():
	_reset_attack_timer()

func _on_attack_cooldown_timeout():
	attack.emit(self)
	attackCooldown.stop()
	animationTimer.wait_time = attackDuration
	animationTimer.start()

#METHOD THAT WILL HANDLE ANIMATIONS, IT WILL RECIEVE FROM PLAYER THE DIRECTION (OF PLAYER OR ARROW)
#AND WITH A MATCH (SW-CASE) PLAY THE ANIMATION ACCORDINGLY
func play_animation(direction):
	match direction:
		"right":
			weaponAnimation.play("right")
		"left":
			weaponAnimation.play("left")
		"up":
			weaponAnimation.play("up")
		"down":
			weaponAnimation.play("down")
		"up_right":
			weaponAnimation.play("up_right")
		"up_left":
			weaponAnimation.play("up_left")
		"down_left":
			weaponAnimation.play("down_left")
		"down_right":
			weaponAnimation.play("down_right")
