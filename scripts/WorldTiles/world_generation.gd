extends Node2D

signal is_loaded
@onready var player = get_tree().get_first_node_in_group("player")
@onready var tileGroup = get_tree().get_first_node_in_group("WorldGenerationGrid")
@onready var worldLimitArea =  $worldLimitArea
@onready var centerMark = $worldLimitArea/CenterMark
@onready var worldReference = $worldLimitArea

@onready var timerTop = $worldLimitArea/TimerTop

@onready var timerDown = $worldLimitArea/TimerDown

@onready var timerLeft = $worldLimitArea/TimerLeft

@onready var timerRight = $worldLimitArea/TimerRight

var tile1 = preload("res://Prefabs/WorldTiles/Tile_1.tscn")

var tile_list = [
	tile1
]

var map_name = null


signal limit_reached

var player_grid_x = 0
var player_grid_y = 0
var tabla = []

const SIZE_GRID_MAP_NEGATIVE_X = -99
const SIZE_GRID_MAP_NEGATIVE_Y = -99

const SIZE_GRID_MAP_POSITIVE_X = 99
const SIZE_GRID_MAP_POSITIVE_Y = 99

const SIZE_TILEMAP = 496

const X_LEFT = -1 
const Y_UP = -1
const X_RIGHT = 1
const Y_DOWN = 1
const DONT_MOVE = 0

func _ready():
	for x in range(SIZE_GRID_MAP_POSITIVE_X):
		var fila = []
		for y in range(SIZE_GRID_MAP_POSITIVE_Y):
			fila.append(false)
		tabla.append(fila)

	call_deferred("append_tile_map",X_LEFT,Y_UP)
	call_deferred("append_tile_map",DONT_MOVE,Y_UP)
	call_deferred("append_tile_map",X_RIGHT,Y_UP)

	call_deferred("append_tile_map",X_LEFT,DONT_MOVE)
	call_deferred("append_tile_map",DONT_MOVE,DONT_MOVE)
	call_deferred("append_tile_map",X_RIGHT,DONT_MOVE)

	call_deferred("append_tile_map",X_RIGHT,Y_DOWN)
	call_deferred("append_tile_map",X_LEFT,Y_DOWN)
	call_deferred("append_tile_map",DONT_MOVE,Y_DOWN)
	
	emit_signal("is_loaded")

func append_tile_map(x_pos,y_pos):
	if (player_grid_x == SIZE_GRID_MAP_NEGATIVE_X):
		player.position.x = worldReference.position.x * -1
		update_world_limit_area_X(-(player_grid_x)*2)
		append_tile_map(DONT_MOVE,DONT_MOVE)
	if (player_grid_x == SIZE_GRID_MAP_POSITIVE_X):
		player.position.x = worldReference.position.x * -1
		update_world_limit_area_X(-(player_grid_x)*2)
	if (player_grid_x == SIZE_GRID_MAP_NEGATIVE_Y):
		player.position.y = worldReference.position.y * -1
		update_world_limit_area_X(-(player_grid_y)*2)
	if (player_grid_x == SIZE_GRID_MAP_POSITIVE_Y):
		player.position.y = worldReference.position.y * -1
		update_world_limit_area_X(-(player_grid_y)*2)
	var x = player_grid_x + (x_pos)
	var y = player_grid_y + (y_pos)

	if !tabla[x][y]:
		var tile = get_random_tile_map()
		var worldTile = tile1.instantiate()
		worldTile.position.x = (worldReference.position.x + (SIZE_TILEMAP*x_pos))
		worldTile.position.y = (worldReference.position.y + (SIZE_TILEMAP*y_pos))
		get_node("TilesWorld").add_child(worldTile)
		tabla[x][y] = true
	else:
		return

func get_random_tile_map():
	var tile_random = ArrayTileMaps.tileMapsPath.pick_random()
	return tile_random

func _on_area_2d_body_entered(body):
	#Top Collision Area
	timerTop.start()
#	append_tile_map(DONT_MOVE,Y_UP)
#	append_tile_map(X_LEFT,Y_UP)
#	append_tile_map(X_RIGHT,Y_UP)
	
func _on_area_2d_2_body_entered(body):
	#Right Collision Area
	timerRight.start()
#	append_tile_map(X_RIGHT,Y_UP)
#	append_tile_map(X_RIGHT,DONT_MOVE)
#	append_tile_map(X_RIGHT,Y_DOWN)

func _on_area_2d_3_body_entered(body):
	#Down Collision Area
	timerDown.start()
#	append_tile_map(X_LEFT,Y_DOWN)
#	append_tile_map(DONT_MOVE,Y_DOWN)
#	append_tile_map(X_RIGHT,Y_DOWN)

func _on_area_2d_4_body_entered(body):
	#Left Collision Area
	timerLeft.start()
#	append_tile_map(X_LEFT,Y_UP)
#	append_tile_map(X_LEFT,DONT_MOVE)
#	append_tile_map(X_LEFT,Y_DOWN)

func update_world_limit_area_y(y_position):
	player_grid_y+=y_position
	worldLimitArea.position.y = SIZE_TILEMAP * player_grid_y
func update_world_limit_area_X(x_position):
	player_grid_x+=x_position
	worldLimitArea.position.x = SIZE_TILEMAP * player_grid_x

func _on_timer_top_timeout():
	call_deferred("append_tile_map",X_LEFT,Y_UP)
	call_deferred("append_tile_map",DONT_MOVE,Y_UP)
	call_deferred("append_tile_map",X_RIGHT,Y_UP)
	update_world_limit_area_y(Y_UP)

func _on_timer_down_timeout():
	call_deferred("append_tile_map",X_LEFT,Y_DOWN)
	call_deferred("append_tile_map",DONT_MOVE,Y_DOWN)
	call_deferred("append_tile_map",X_RIGHT,Y_DOWN)
	update_world_limit_area_y(Y_DOWN)

func _on_timer_right_timeout():
	call_deferred("append_tile_map",X_RIGHT,Y_UP)
	call_deferred("append_tile_map",X_RIGHT,DONT_MOVE)
	call_deferred("append_tile_map",X_RIGHT,Y_DOWN)
	update_world_limit_area_X(X_RIGHT)

func _on_timer_left_timeout():
	call_deferred("append_tile_map",X_LEFT,Y_UP)
	call_deferred("append_tile_map",X_LEFT,DONT_MOVE)
	call_deferred("append_tile_map",X_LEFT,Y_DOWN)
	update_world_limit_area_X(X_LEFT)
