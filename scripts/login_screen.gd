extends Control
@onready var button = $PanelContainer/VBoxContainer/BtPlay
@onready var userLe = $PanelContainer/VBoxContainer/UsernameLE
@onready var pswdLe = $PanelContainer/VBoxContainer/PasswordLE

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_bt_play_pressed():
	button.disabled = true
	var username = userLe.text
	var password = pswdLe.text
	var request = get_node("HTTPRequest")
	request.http_post_request(username, password)
	print(request)

func _on_guest_lb_pressed():
	get_tree().change_scene_to_file("res://main_menu.tscn")


func _on_create_acc_lb_pressed():
	get_tree().change_scene_to_file("res://Screens/register_screen.tscn")
