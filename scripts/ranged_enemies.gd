extends CollisionShape2D

@onready var player = get_tree().get_first_node_in_group("player")
@onready var ranged = get_tree().get_first_node_in_group("ranged_mob")
@onready var arrow = preload("res://Prefabs/Unlockable/Spells/Enemies_projectiles.tscn")

func _process(delta):
	on_archer_mob_can_shoot()

func on_archer_mob_can_shoot():
	pass
