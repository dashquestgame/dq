extends Node

@export var spells : Array[String] = ["Fireball","Magic Bolt","Cosmic Cluster","Dark Domain"] 

var magicBolt = preload("res://Prefabs/Unlockable/Spells/MagicBolt.tscn")
var fireball = preload("res://Prefabs/Unlockable/Spells/Fireball.tscn")
var cosmicCluster = preload("res://Prefabs/Unlockable/Spells/CosmicCluster.tscn")
var darkDomain = preload("res://Prefabs/Unlockable/Spells/DarkDomain.tscn")

var darkDomainNode = darkDomain.instantiate()
var fireballNode = fireball.instantiate()
var magicBoltNode = magicBolt.instantiate()
var cosmicClusterNode = cosmicCluster.instantiate()

@onready var player = get_tree().get_first_node_in_group("player")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func attachSpell(spell):
	match spell:
		"Fireball":
			player.get_node("UnlockedItems").add_child(fireballNode)
		"Magic Bolt":
			player.get_node("UnlockedItems").add_child(magicBoltNode)
		"Cosmic Cluster":
			player.get_node("UnlockedItems").add_child(cosmicClusterNode)
		"Dark Domain":
			player.get_node("UnlockedItems").add_child(darkDomainNode)
	
func getRandomSpell():
	var aux = randi_range(0,spells.size()-1)
	return(spells[aux])		
	
