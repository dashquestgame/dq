extends Node

@export var spear = preload("res://Prefabs/Unlockable/Weapons/Spear.tscn")
@export var sword = preload("res://Prefabs/Unlockable/Weapons/Sword.tscn")

var spearNode = spear.instantiate()
var swordNode = sword.instantiate()

@onready var player = get_tree().get_first_node_in_group("player")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func attachWeapon(weapon):
	match weapon:
		"Spear":
			player.get_node("Weapons").add_child(spearNode)
		"Sword":
			player.get_node("Weapons").add_child(swordNode)
